# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160126145901) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "access_tokens", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "access_tokens", ["token"], name: "index_access_tokens_on_token", using: :btree
  add_index "access_tokens", ["user_id"], name: "index_access_tokens_on_user_id", using: :btree

  create_table "comments", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "ticket_id"
    t.string   "comment_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.boolean  "first_comment"
  end

  add_index "comments", ["comment_id"], name: "index_comments_on_comment_id", using: :btree
  add_index "comments", ["ticket_id"], name: "index_comments_on_ticket_id", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "constants", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "ticket_linkeds", force: :cascade do |t|
    t.integer  "ticket_id"
    t.integer  "follower_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "ticket_linkeds", ["follower_id"], name: "index_ticket_linkeds_on_follower_id", using: :btree
  add_index "ticket_linkeds", ["ticket_id"], name: "index_ticket_linkeds_on_ticket_id", using: :btree

  create_table "tickets", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "ticket_id"
    t.integer  "project_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "tickets", ["user_id"], name: "index_tickets_on_user_id", using: :btree

  create_table "transactions", force: :cascade do |t|
    t.integer  "ticket_id"
    t.string   "status"
    t.string   "action_type"
    t.text     "content"
    t.integer  "performer_id"
    t.string   "performer_name"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "message"
    t.string   "highlight"
  end

  add_index "transactions", ["ticket_id"], name: "index_transactions_on_ticket_id", using: :btree

  create_table "user_tickets", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "ticket_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "user_tickets", ["ticket_id"], name: "index_user_tickets_on_ticket_id", using: :btree
  add_index "user_tickets", ["user_id"], name: "index_user_tickets_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "name",         limit: 50
    t.string   "email"
    t.string   "google_id"
    t.integer  "access_level", limit: 2
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "image"
    t.integer  "pivotal_id"
  end

  create_table "webhooks", force: :cascade do |t|
    t.text     "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "access_tokens", "users"
end
