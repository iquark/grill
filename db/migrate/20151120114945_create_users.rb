class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name, :limit => 50
      t.string :email
      t.string :google_id
      t.integer :access_level, :limit => 2 # smallint

      t.timestamps null: false
    end
  end
end