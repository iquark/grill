class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.belongs_to :user, index: true
      t.belongs_to :ticket, index: true

      t.string :comment_id, index: true

      t.timestamps null: false
    end
  end
end
