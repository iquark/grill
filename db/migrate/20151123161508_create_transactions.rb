class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.belongs_to :ticket, index:true
      t.string :status
      t.string :action_type
      t.text :content
      t.integer :performer_id
      t.string :performer_name

      t.timestamps null: false
    end
  end
end
