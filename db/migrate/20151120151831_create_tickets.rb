class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.belongs_to :user, index:true
      t.integer :ticket_id
      t.integer :project_id

      t.timestamps null: false
    end
  end
end
