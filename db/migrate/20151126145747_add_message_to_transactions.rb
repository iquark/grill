class AddMessageToTransactions < ActiveRecord::Migration
  def change
    add_column :transactions, :message, :string
    add_column :transactions, :highlight, :string
  end
end
