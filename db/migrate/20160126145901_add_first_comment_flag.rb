class AddFirstCommentFlag < ActiveRecord::Migration
  def change
    add_column :comments, :first_comment, :boolean, :default => nil
  end
end
