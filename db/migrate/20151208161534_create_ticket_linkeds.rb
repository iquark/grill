class CreateTicketLinkeds < ActiveRecord::Migration
  def change
    create_table :ticket_linkeds do |t|

      t.references :ticket, index: true
      t.references :follower, index: true

      t.timestamps null: false
    end
  end
end
