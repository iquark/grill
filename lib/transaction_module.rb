module TransactionModule
  include NotificationModule

  ENDPOINT = Team.endpoint

  # Stores a new transaction into the DB
  def store_transaction(data)
    unless data.nil?
      transaction = Transaction.create data
      # Creates the notification for a user (v2) or team (v1) if required
      if not transaction.nil?
        case transaction[0].action_type
          when "story_create_activity" then
            labels = JSON.parse(transaction[0].content)['new_values']['labels']
            severity = 'low'
            to = nil
            labels.each do |l|
              severity = l if Ticket.severities.include? l
              to = {team: l} if Team.teams.include? l
            end
            notify(to, data, severity) unless to.nil?
          when "comment_create_activity" then
            if JSON.parse(transaction[0].content)['kind'] == 'comment'
              # if the comment has a ticket ID, check if that exists
              text = JSON.parse(transaction[0].content)['new_values']['text']
              story_id = JSON.parse(transaction[0].content)['new_values']['story_id']
              ids = text.match(/\#[0-9]+/)

              unless ids.nil? or ids.length == 0

                ids.to_a.each do |ticket_id|
                  ticket_id = ticket_id.split('#')[1]
                  # if the ticket exists in our database, and is not linked
                  ticket = Ticket.find_by ticket_id: ticket_id

                  unless ticket.nil?
                    linked = TicketLinked.find_by ticket_id: story_id, follower_id: ticket_id
                    if linked.nil?
                      TicketLinked.create ticket_id: story_id, follower_id: ticket_id
                    end
                  else
                    # checking if ticket exists on assigned team
                    new_ticket = {
                      ticket_id: ticket_id,
                      project_id: nil
                    }

                    # retrieve the team id to get the project ID
                    Unirest.default_header('X-TrackerToken', ENV['PIVOTAL_TOKEN'])
                    Unirest.default_header('Content-Type', 'application/json')
                    story = Unirest.get("#{ENDPOINT}/projects/#{ENV['PIVOTAL_PROJECT']}/stories/#{story_id}")

                    if story.code == 200
                      story = story.body
                      severity = nil
                      to = nil
                      story["labels"].each do |l|
                        severity = l["name"] if Ticket.severities.include? l["name"]
                        to = {team: l["name"]} if Team.teams.include? l["name"]
                      end
                      project_id = Rails.configuration.x.slack_integrations[to[:team]]['project_id']

                      # now try to get the project from both
                      response = Unirest.get("#{ENDPOINT}/stories/#{ticket_id}")
                      if response.code == 200
                        new_ticket[:project_id] = response.body['project_id']
                      end
                    end

                    # if ticket exists then it will be created and linked to the current
                    unless new_ticket[:project_id].nil?
                      ticket = Ticket.create(new_ticket)
                      TicketLinked.create ticket_id: story_id, follower_id: ticket_id
                    end
                  end
                end
              end
            end
          when "story_delete_activity" then
            transaction.each do |trans|
              content = JSON.parse(trans.content)

              if content['kind'] == 'story'
                ticket = Ticket.find_by(ticket_id: content['id'])
                unless ticket.nil?
                  Comment.where(ticket_id: ticket['id']).destroy_all
                  Ticket.where(ticket_id: content['id']).destroy_all
                end
              end
            end
          when "comment_delete_activity"
            transaction.each do |trans|
              content = JSON.parse(trans.content)

              if content["kind"] == "comment"
                Comment.where(comment_id: content["id"]).destroy_all
              end
            end
        end
      end
    end
  end

  # Returns a list of the last transactions related to tickets the
  # user is owner of or is following to
  #
  # @param user_id User ID the digest is for
  # @TODO on v2
  def get_digest(user_id)
#    Transaction.order("created_on ASC").where(updated_at: (Time.now - 24.hours)..Time.now).find_by(performer_id: user_id)
    [{}]
  end

  # Format a list of activities to our purposes
  def format_activities(activities)
    history = []
    severity = ""
    team = ""
    latest = nil
    icon_pivotal = ActionController::Base.helpers.asset_path("icons/pivotal-tracker.svg")
    icon_grill = ActionController::Base.helpers.asset_path("logo.png")

    activities = activities.reverse

    activities.each do |activity|

      case activity["kind"]
      when "story_create_activity"
        activity["changes"].each do |change|
          if change["kind"] == "story"
            line = {
              "text" => "Ticket created",
              "kind" => "transaction",
              "created_at" => change["new_values"]["updated_at"],
              "data" => change,
              "icon" => icon_pivotal
            }
            history.push line
            change["new_values"]["labels"].each do |label|
              if Ticket.severities.include?(label)
                severity = label
                line = {
                  "text" => "Set as <em>#{label}</em>",
                  "kind" => "transaction",
                  "created_at" => change["new_values"]["updated_at"],
                  "data" => change,
                  "icon" => icon_pivotal
                }
                history.push line
              else
                team = label
                line = {
                  "text" => "Assigned to <em>#{label}</em>",
                  "kind" => "transaction",
                  "created_at" => change["new_values"]["updated_at"],
                  "data" => change,
                  "icon" => icon_pivotal
                }
                history.push line
              end
            end
            break
          end
        end
      when "story_update_activity"
        case activity["highlight"]
        when "edited"
          activity["changes"].each do |change|
            if change["kind"] == "story"
              unless change["new_values"]["labels"].nil?
                change["new_values"]["labels"].each do |label|
                  if Ticket.severities.include?(label)
                    if severity!=label
                      severity = label
                      line = {
                        "text" => "Set as <em>#{label}</em>",
                        "kind" => "transaction",
                        "created_at" => change["new_values"]["updated_at"],
                        "data" => change,
                        "icon" => icon_pivotal
                      }
                      history.push line
                    end
                  elsif team != label
                    team = label
                    line = {
                      "text" => "Assigned to <em>#{label}</em>",
                      "kind" => "transaction",
                      "created_at" => change["new_values"]["updated_at"],
                      "data" => change,
                      "icon" => icon_pivotal
                    }
                    history.push line
                  end
                end
              end
            end
          end
        when "started", "edited", "accepted", "rejected", "delivered", "finished", "unstarted"
          activity["changes"].each do |change|
            if change["kind"] == "story"
              line = {
                "text" => "Task changed to <em>#{change["new_values"]["current_state"]}</em>",
                "kind" => "transaction",
                "created_at" => change["new_values"]["updated_at"],
                "data" => change,
                "icon" => icon_pivotal
              }
              history.push line
            end
          end
        end
      end
    end

    history
  end

  def merge_activities_comments(activities, comments=[])
    aidx = 0
    cidx = 0
    result = []
    max_length = comments.length + activities.length
    icon_pivotal = ActionController::Base.helpers.asset_path("icons/pivotal-tracker.svg")
    icon_grill = ActionController::Base.helpers.asset_path("logo.png")
    users = {}

    while result.length < max_length
      # if we have put all comments we had
      if cidx >= comments.length
        result.concat activities.slice aidx, activities.length
        aidx = activities.length

      # if we have put all activities we had
      elsif aidx >= activities.length
        ids = comments[cidx]["text"].match(/\#[0-9]{9}/)
        unless ids.nil? or ids.length == 0
          line = {
            "text" => "Task <em>#{ids[0]}</em> created",
            "kind" => "transaction",
            "created_at" => comments[cidx]["created_at"],
            "data" => comments[cidx],
            "icon" => icon_pivotal
          }
          result.push(line)
        else
          user_icon = icon_pivotal
          comments[cidx]["icon"] = user_icon
          result.push(comments[cidx])
        end
        cidx+=1

      # if we still have comments and activities to place
      else
        atime = Time.parse(activities[aidx]["data"]["new_values"]["updated_at"])
        ctime = Time.parse(comments[cidx]["created_at"])
        if atime < ctime
          result.push(activities[aidx])
          aidx+=1
        else
          ids = comments[cidx]["text"].match(/\#[0-9]+/)

          unless ids.nil? or ids.length == 0
            line = {
              "text" => "Task <em>#{ids[0]}</em> created",
              "kind" => "transaction",
              "created_at" => comments[cidx]["created_at"],
              "data" => comments[cidx],
              "icon" => icon_pivotal
            }
            result.push(line)
          else
            user_icon = icon_pivotal
            comments[cidx]["icon"] = user_icon
            result.push(comments[cidx])
          end
          cidx+=1
        end
      end
    end
    result
  end

  def get_attachments_from_activities(activities)
    attachments = {}

    activities = activities.reverse

    activities.each do |activity|
      if activity["kind"] == "comment_create_activity"
        activity["changes"].each do |change|
          if change["kind"] == "file_attachment"
            attachments[change["id"]] = change
          end
        end
      elsif activity["kind"] == "comment_delete_activity"
        activity["changes"].each do |change|
          if change["kind"] == "file_attachment"
            attachments.delete(change["id"])
          end
        end
      end
    end

    attachments
  end

end
