module WebhookModule
  include TransactionModule

  # Checks if a given webhook is white-listed and creates the transaction
  # if is necessary, a notification would be created
  def store_webhook(webhook)

    unless webhook.nil?
      ticket = Ticket.find_by ticket_id: webhook[:primary_resources][0][:id]

      unless ticket.nil?
        data = []
        webhook[:changes].each do |change|
          if change[:kind] == "story" or change[:kind] == "comment"
            unless webhook[:primary_resources][0][:url].nil?
              change[:url] = webhook[:primary_resources][0][:url]
            end

            t = {
              ticket_id: ticket[:id],
              highlight: webhook[:highlight],
              action_type: webhook[:kind],
              content: change.to_json,
              message: webhook[:message],
              performer_id: webhook[:performed_by][:id],
              performer_name: webhook[:performed_by][:name]
            }

            unless change[:new_values].nil? or change[:new_values][:current_state].nil?
              t['status'] = change[:new_values][:current_state]
            end

            data.push(t)
          end
        end
        store_transaction data
      end
    end
  end
end
