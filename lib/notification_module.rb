module NotificationModule

  # Real time notification to a user
  # @param to User or team the notification is for
  def notify(to, data, severity = 'low')
    unless to[:user].nil?
      # send notification to the user
      # @TODO on v2
    end
    unless to[:team].nil?
      # send notification to the team, they are usually notified via Slack
      send_by_slack(to, data, severity)
    end
  end

  # Sends a digest to a user
  # @param user_id User ID the digest is for
  # @return boolean if the digest was or no successfully sent
  # @TODO on v2
  def send_digest(user_id)
    # Transaction.get_digest user_id
    # get the mean the user should be notified
    # send_by_email | send_by_slack
    true
  end

  private

  def send_by_email(to, data, severity)
    unless to[:user].nil?
      # send notification to the user
    end
    unless to[:team].nil?
      # send notification to the team
    end
  end

  def send_by_slack(to, data, severity)
    content = JSON.parse(data[0][:content])

    unless content['url'].nil?
      message = "*<#{content['url']}|#{content['new_values']['name']}>*\n"
    else
      message = "*#{content['new_values'][:name]}*\n"
    end

    user = nil
    ticket_data = Ticket.find_by ticket_id: content['new_values']['id'] unless content['new_values']['id'].nil?
    user = User.find_by id: ticket_data["user_id"] unless ticket_data.nil?

    if user.nil?
      message = "#{message} #{data[0][:message]}"
    else
      message = "#{message} #{user['name']} added this bug"
    end

    unless to[:user].nil?
      # send notification to the user
    end
    unless to[:team].nil?
      puts "Notification: sending message to #{to[:team]}"
      # send notification to the team
      url = Rails.configuration.x.slack_integrations[to[:team]]['slack']
      unless url.nil?
#        response = Unirest::HttpResponse.new(RestClient::Request.execute
        response = Unirest.post(url, parameters: payload='{"text": "'+message+'",  "username": "Grill", "icon_emoji":"'+Ticket.get_icon(severity)+'"}')

        if not response.nil? and response.code == 200
          puts response.body
        else
          puts response.code, ":", response.body unless response.nil?
        end
      end
    end
  end

end
