module TicketModule
  require 'mime/types'
  require 'json'
  include TransactionModule

  ENDPOINT = Team.endpoint

  # Retrieves all tickets
  def get_all
    response = Unirest.get("#{ENDPOINT}/projects/#{ENV['PIVOTAL_PROJECT']}/stories").body
    response
  end

  def filter_by_tickets(filter, tickets, opened=true)
    list = []
    filter = filter.map {|v| v[:ticket_id]}
    statuses = []

    if opened
      statuses = Ticket.opened_statuses
    else
      statuses = Ticket.closed_statuses
    end

    tickets.each do |ticket|
      list.push ticket if filter.include?(ticket["id"]) and statuses.include?(ticket["current_state"])
    end

    list
  end

  def filter_by_status(tickets, opened=true)
    list = []

    if opened
      statuses = Ticket.opened_statuses
    else
      statuses = Ticket.closed_statuses
    end

    tickets.each do |ticket|
      list.push ticket if statuses.include?(ticket["current_state"])
    end

    list
  end

  def filter_by_team(team, tickets)
    list = []
    numbers = {
      'all': tickets.length,
      'ios': 0,
      'android': 0,
      'web': 0,
      'platform': 0,
      'techops': 0
    }

    tickets.each do |ticket|
      unless ticket["labels"].nil?
        ticket["labels"].each do |label|
          unless Ticket.severities.include?(label['name'].downcase)
            list.push ticket if label["name"] == team or team == 'all'
            numbers[:ios] += 1 if label["name"] == 'ios'
            numbers[:android] += 1 if label["name"] == 'android'
            numbers[:web] += 1 if label["name"] == 'web'
            numbers[:platform] += 1 if label["name"] == 'platform'
            numbers[:techops] += 1 if label["name"] == 'techops'
          end
        end
      end
    end

    {"list": list, "numbers": numbers}
  end

  # Sort Tickets by the state, according the order in Ticket model
  def sort_tickets_by_state(tickets)
    statuses = {}
    list = []
    ticket_statuses = []

    tickets.each do |ticket|
      if statuses[ticket["current_state"]].nil?
        statuses[ticket["current_state"]] = []
      end
      statuses[ticket["current_state"]].push ticket
    end

    ticket_statuses = Ticket.all_statuses
    alias_statuses = Ticket.alias_all_statuses

    ticket_statuses.each do |s|
      list.concat statuses[s] unless statuses[s].nil? 
    end

    list
  end

  # sorting tickets by the last update (quicksort)
  def sort_tickets_by_last_update(tickets)
    list = sort! tickets

    list
  end

  # Retrieves all projects the superuser owns
  def get_projects
    unless session[:user].nil?
      if session[:user]["projects"].nil?
        session[:user]["projects"] = Unirest.get("#{ENDPOINT}/projects").body.map {|i| [i['name'],i['id']] }
      end
      session[:user]["projects"]
    else
      []
    end
  end

  # Retrieves the ticket information from the API
  # @param ticket_id The ticket ID on PT
  # @param step      Step
  def get_ticket(ticket_id, simple=false, step=nil)
    ticket_data = Ticket.find_by ticket_id: ticket_id
    ticket = Unirest.get("#{ENDPOINT}/stories/#{ticket_id}").body

    unless ticket.nil? or ticket['kind'] == 'error'
      ticket["name"] = Rumoji.decode(ticket["name"])
      ticket["description"] = Rumoji.decode(ticket["description"])
      ticket['severity'] = 'low'
      ticket['team'] = 'unknown'

      unless ticket["labels"].nil?
        ticket["labels"].each do |label|
          if Ticket.severities.include?(label['name'].downcase)
            ticket['severity'] = label["name"]
          else
            ticket['team'] = label["name"]
          end
        end
      end

      unless simple
        ticket['requester'] = get_member ticket['requested_by_id']
        ticket['owners'] = get_owners ticket_id, ticket["project_id"]
        comments = get_comments ticket_id

        # get the transactions history
        activities = get_activity ticket_id, ticket["project_id"]
        # if a comment with the 'first_comment' flag exists, its attachments must be
        # placed in the top list
        first_comment_id = nil
        comments.each do |comment|
          unless comment['first_comment'].nil?
            first_comment_id = comment['id']
            break
          end
        end

        ticket['attachments'] = get_attachments ticket_id, ticket["project_id"], first_comment_id
        activities = format_activities(activities)
        ticket['history']  = merge_activities_comments activities, comments
      end
    else
      ticket = nil
    end

    unless ticket_data.nil?
      # if a step is required, and there are transactions, the ticket should be
      # returned at the given step, rolling back the changes
      unless step.nil?
        # @TODO not in v1
      end
    end
    ticket
  end

  def get_attachments(ticket_id, project_id, first_comment_id = nil)
    list = Unirest.get("#{ENDPOINT}/projects/#{project_id}/stories/#{ticket_id}/comments?fields=file_attachments").body
    attachments = {}

    list.each do |item|
      if item['file_attachments'].length>0
        if item['id'] == first_comment_id
          attachments['top'] = item['file_attachments']
        else
          attachments[item['id']] = item['file_attachments']
        end
      end
    end

    attachments
  end

  # retrieve the owners of a ticket
  def get_owners(ticket_id, project_id)
    Unirest.get("#{ENDPOINT}/projects/#{project_id}/stories/#{ticket_id}/owners").body
  end

  # Saves the ticket ID in the TT project
  def store_ticket(data)
    # rumoji encodes the emojis to characters supported by PT
    name = Rumoji.encode(data[:name].strip)
    description = Rumoji.encode(data[:description].strip)

    validation = Ticket.validate(name, description)

    unless validation[:errors].nil?
      validation[:ticket] = data

      validation
    else
      parameters = {
        name: name,
        description: description,
        labels: [data[:team], data[:severity]],
        story_type: 'bug'
      }

      project_id = unless data[:project_id].nil? then data[:project_id] else ENV['PIVOTAL_PROJECT'] end

      p_response = Unirest.post "#{ENDPOINT}/projects/#{project_id}/stories",
                          parameters: parameters

      # when an error happens labels are stored in an array to be shown in the view
      if p_response.code != 200 and p_response.body["kind"] == "error"
        data = {
          labels: [
            {name: data[:team]},
            {name: data[:severity]}
          ]
        }

        errors = {}
        unless p_response.body["validation_errors"].nil?
          errors = p_response.body["validation_errors"]
        else
          errors = { "general": p_response.body["error"]} unless p_response.body["error"].nil?
        end

        {errors: errors, ticket: data}
      else
        ticket = Ticket.create({
          ticket_id: p_response.body["id"],
          project_id: p_response.body["project_id"],
          user_id: session[:user]["id"]
        })

        # add file attachments
        unless data[:attachments].nil?
          comment = {
            text: "attached the following files",
            file_attachments: data[:attachments],
            project_id: p_response.body["project_id"],
            story_id: p_response.body["id"]
          }
          store_comment comment, true
        end
        
        {ticket: ticket}
      end
    end
  end

  # updates a given ticket
  def update_ticket(ticket_id, data)
    data["story_type"] = 'bug'
    data[:id] = ticket_id

    # rumoji encodes the emojis to characters supported by PT
    data[:name] = Rumoji.encode(data[:name].strip)
    data[:description] = Rumoji.encode(data[:description].strip)

    validation = Ticket.validate(data[:name], data[:description])

    unless validation[:errors].nil?
      validation[:ticket] = data

      validation
    else
      p_response = Unirest.put "#{ENDPOINT}/projects/#{ENV['PIVOTAL_PROJECT']}/stories/#{ticket_id}",
        parameters: data

      # when an error happens labels are stored in an array to be shown in the view
      if p_response.code != 200 and p_response.body["kind"] == "error"
        data[:labels] = [
          {name: data[:team]},
          {name: data[:severity]}
        ]
        data[:id] = ticket_id
        errors = p_response.body["validation_errors"]

        {errors: errors, ticket: data}
      else
        ticket = Ticket.find_by(ticket_id: ticket_id)

        unless ticket.nil?
          if ticket[:project_id] != p_response.body["project_id"]
            ticket.project_id = p_response.body["project_id"]
          end

          ticket.touch(:updated_at)

          ticket.save
        end

        {ticket: ticket}
      end
    end
  end

  # Retrieve comments of a ticket
  def get_comments(ticket_id)
    comments =[]
    ticket = Ticket.find_by ticket_id: ticket_id

    project_id = ENV['PIVOTAL_PROJECT']

    unless ticket.nil?
      project_id = ticket[:project_id]
    end

    response = Unirest.get("#{ENDPOINT}/projects/#{project_id}/stories/#{ticket_id}/comments")

    if not response.nil? and response.code == 200
      id = if ticket.nil? or ticket[:id].nil? then nil else ticket[:id] end
      comments = format_comments id, response.body
    end
    comments
  end

  # makes all necessary changes into the comments to be properly shown
  def format_comments ticket_id, comments
    # retrieve our comments from DB
    our_comments = Comment.select('comments.*, users.name, users.email').joins(:user).where(ticket_id: ticket_id)

    # attach users
    members = get_members
    comments.each do |comment|
      # format the emojis
      comment["text"] = Rumoji.decode(comment["text"])

      found = false

      unless our_comments.nil?
        our_comment = our_comments.find {|c|
          c["comment_id"].to_i == comment['id'].to_i
        }
        unless our_comment.nil?
          comment[:author] = {
            "person" => {
              "name" => our_comment['name'],
              "email" => our_comment['email'],
              "username" => our_comment['email'].split('@')[0]
            }
          }
          # when we store a comment, to make the people know who is we prepend the name of the user who sent the comment,
          # we remove it here as we have got the name
          pre_name = "#{our_comment['name']}: " 
          comment["text"] = comment["text"].sub(pre_name, '') if comment["text"].start_with?(pre_name)
          comment["first_comment"] = true unless our_comment['first_comment'].nil?
          found = true
        end
      end

      # if we do not own the comment, it will use the user from PT
      unless found
        # if we have a user on our records, that one has the preference
        user = User.find_by pivotal_id: comment['person_id']

        unless user.nil?
          comment[:author] = {
            "person" => {
              "name" => user['name'],
              "email" => user['email'],
              "username" => user['email'].split('@')[0]
            }
          }
        else
          # otherwise we will use the users from PT
          member = members.select {|m|
            m['id'] == comment['person_id']
          }
          if member.length>0
            comment[:author] = member[0]
          end
        end
      end
    end

    comments
  end

  # Stores a new comment in a ticket
  # Parameters in object:
  # @param story_id  Story ID
  # @param text      Text to show
  # @param project_id Project where the story is
  # @param file_attachments List of files attached
  def store_comment(data, first = false)
    username = session[:user]["name"]
    id = session[:user]["id"]
    ret = {ticket: data}
    data[:text] = "#{username}: #{Rumoji.encode(data[:text])}"  # rumoji encodes the emojis to characters supported by PT

    unless data[:text].length > Ticket.max_comment
      response = Unirest.post "#{ENDPOINT}/projects/#{data[:project_id]}/stories/#{data[:story_id]}/comments", parameters: data.to_json

      if response.code != 200 and response.body["kind"] == "error"
        errors = response.body["validation_errors"]
        ret[:errors] = errors
      else
        if response.body["kind"] == "comment"
          ticket = Ticket.find_by ticket_id: data[:story_id]

          unless ticket.nil?
            comment_data = {
              user_id: session[:user]["id"],
              ticket_id: ticket[:id],
              comment_id: response.body["id"]
            }
            comment_data[:first_comment] = true if first
            Comment.create (comment_data)
          end
        end
      end

    else
      ret[:errors] = [{"field" => "comment", "problem" => "The comment can not exceed #{Ticket.max_comment} characters."}]
    end
    ret
  end

  # Uploads a file to Pivotal Tracker
  def upload_file(file, delete = false)
    path = File.absolute_path(file)
    Unirest.default_header('Content-Type', MIME::Types.type_for(path).first.to_s)
    Unirest.default_header('Content-Disposition', 'form-data; name="file"; filename="'+path+'"')

    response = Unirest.post "#{ENDPOINT}/projects/#{ENV['PIVOTAL_PROJECT']}/uploads",
                        parameters:{ :file => file }

    File.delete path if delete

    response.body
  end

  # Downloads a file from Pivotal Tracker
  def download_file(file_attachment_id)
    response = Unirest.get("https://www.pivotaltracker.com/file_attachments/#{file_attachment_id}/download")

    content = nil
    filename = "empty.png"
    if not response.nil? and response.code == 200
      filename = response.headers[:content_disposition].split('filename=')[1].gsub('"', '')
      content = response.body
    end

    {filename: filename, content: content}
  end

  # Retrieve all the activity of a ticket from pivotal tracker
  def get_activity(ticket_id, project_id)
    response = Unirest.get("#{ENDPOINT}/projects/#{project_id}/stories/#{ticket_id}/activity")

    activity = []
    if not response.nil? and response.code == 200
      activity = response.body
    end

    activity
  end

  def get_member(person_id)
    Unirest.get("#{ENDPOINT}/accounts/#{ENV['PIVOTAL_ACCOUNT']}/memberships/#{person_id}").body
  end

  def get_members
    Unirest.get("#{ENDPOINT}/accounts/#{ENV['PIVOTAL_ACCOUNT']}/memberships").body
  end

  private
  def sort!(keys)
    quick(keys,0,keys.size-1)
  end
    
  private
  
  def quick(keys, left, right)
    if left < right
      pivot = partition(keys, left, right)
      quick(keys, left, pivot-1)
      quick(keys, pivot+1, right)
    end
    keys
  end
    
  def partition(keys, left, right)
    x = Time.parse(keys[right]["updated_at"])
    i = left-1
    for j in left..right-1
      if Time.parse(keys[j]["updated_at"]) > x
        i += 1
        keys[i], keys[j] = keys[j], keys[i]
      end
    end
    keys[i+1], keys[right] = keys[right], keys[i+1]
    i+1
  end

end
