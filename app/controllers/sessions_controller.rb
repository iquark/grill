class SessionsController < ApplicationController

  def create
    user = User.from_omniauth(auth_hash)
    access_token = AccessToken.create(user: user)

    cookies[:auth_token] = {
       :value => access_token.token,
       :expires => 2.weeks.from_now
       # TODO: once it works on HTTPS
       # :secure => true
       # TODO: after knowing the domain
       #  ,domain: 'domain.com'
     }
    session[:user] = user

    redirect_to tickets_path
  end

  def failure
    redirect_to tickets_path
  end

  def destroy
    access_token = AccessToken.find_by(token: cookies[:auth_token])
    access_token.destroy unless access_token.nil?

    cookies.delete(:auth_token)
    session[:user] = nil
    # TODO: after knowing the domain:
    # cookies.delete(:auth_token, domain: 'domain.com')
    redirect_to login_path
  end

  protected

  def auth_hash
    request.env['omniauth.auth']
  end
end
