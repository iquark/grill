class WebhooksController < ApplicationController
  include WebhookModule
  protect_from_forgery :except => [:new]

  def index
    @transactions = Transaction.all
  end

  def new
    store_webhook(params)
  end
end
