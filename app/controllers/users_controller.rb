class UsersController < ApplicationController
  layout 'public'
  ENDPOINT = Team.endpoint

  before_filter :default_header

  def index
    if cookies[:auth_token] and not session[:user].nil?
      access_token = AccessToken.find_by({token: cookies[:auth_token], user_id: session[:user]["id"]})

      if access_token.nil?
        cookies.delete(:auth_token)
        session[:user] = nil
        session[:team] = nil
        session[:sort_by] = nil
        session[:status] = nil
        session[:show] = nil
      else
        redirect_to tickets_path
      end
    end
  end
  
  def check_login
    Unirest.default_header('X-TrackerToken', nil)

    @login_info = Unirest.get("#{ENDPOINT}/me", auth:{:user=>params[:username], :password=>params[:password]}).body
    
    if not @login_info.nil? and @login_info["kind"] == "me"
      session[:token] = @login_info["api_token"]
      session[:user] = @login_info
      redirect_to tickets_path
    else
      render 'login_error'
    end

  end

  def logout
    session[:token] = nil
    session[:user] = nil
    session[:team] = nil
    session[:sort_by] = nil
    session[:status] = nil
    session[:show] = nil
    redirect_to login_path
  end

  def default_header
    Unirest.default_header('Content-Type', 'application/json')
  end
end
