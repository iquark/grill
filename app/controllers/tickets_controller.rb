class TicketsController < ApplicationController
  include TicketModule
  include UtilModule

  before_filter :check_login
  before_filter :default_header

  # GET: shows the list of tickets
  def index
    @statuses = Ticket.all_statuses
    @alias_statuses = Ticket.alias_all_statuses

    @team = if session[:team].nil? then 'all' else session[:team] end
    @sort_by = if session[:sort_by].nil? then 'newest' else session[:sort_by] end
    @status = if session[:status].nil? then 'opened' else session[:status] end
    @show = if session[:show].nil? then 'mine' else session[:show] end

    unless params[:team].nil?
      @team = params[:team]
      session[:team] = params[:team]
    end

    unless params[:sortby].nil?
      @sort_by = params[:sortby]
      session[:sort_by] = params[:sortby]
    end

    unless params[:status].nil?
      @status = params[:status]
      session[:status] = params[:status]
    end

    unless params[:show].nil?
      @show = params[:show]
      session[:show] = params[:show]
    end

    @opposite_status = if @status=='opened' then 'closed' else 'opened' end

    @bugs = get_all

    @total = @bugs.length

    # filter my tickets
    if @show == 'mine'
      tickets = Ticket.where(user_id: session[:user]["id"]).select("ticket_id")
      @total = tickets.length

      @bugs = filter_by_tickets tickets, @bugs, @status=='opened'
    else
      @bugs = filter_by_status @bugs, @status=='opened'
    end

    if @sort_by == 'state'
      @bugs = sort_tickets_by_state @bugs
    else
      @bugs = sort_tickets_by_last_update @bugs
    end

    # filter by team
    filter = filter_by_team @team, @bugs
    @bugs = filter[:list]
    @numbers = filter[:numbers]

  end

  # GET: shows to the user the form to create a new ticket
  def new
    @browser = users_browser
    @ticket = {}
    if not flash[:errors].nil?
      @errors = flash[:errors]
      @ticket = flash[:ticket]
    end
  end

  # POST: validates and creates a new ticket
  def create
    @browser = users_browser
    ticket = params[:tickets]

    parameters = {
      name: ticket[:name],
      description: ticket[:description],
      labels: [ticket[:team], ticket[:severity]],
      team: ticket[:team],
      severity: ticket[:severity]
    }

    unless ticket[:file_attachments].nil? or ticket[:file_attachments] ==  "" or ticket[:file_attachments] ==  "[]"
      parameters[:attachments] = ActiveSupport::JSON.decode(ticket[:file_attachments])
    end

    result = store_ticket parameters

    unless result.nil? or result[:errors].nil?
      @ticket = result[:ticket]
      @ticket["name"] = @ticket[:name] if @ticket["name"].nil? and not @ticket[:name].nil?
      @ticket["description"] = @ticket[:description] if @ticket["description"].nil? and not @ticket[:description].nil?
      @errors = result[:errors]
      @attachments = ticket[:file_attachments]

      render 'new'
    else
      session[:status] = 'opened'
      redirect_to tickets_path
    end

  end

  # GET: shows to the user the edit form for a given ticket
  def edit
    @browser = users_browser
    unless flash[:errors].nil?
      @errors = flash[:errors]
      @ticket = flash[:ticket]
    else
      @ticket = get_ticket params[:id], true
    end
  end

  # PUT: updates a ticket using the given data
  def update
    @browser = users_browser
    @errors = nil
    @ticket = params[:tickets]

    parameters = {
      name: @ticket[:name],
      description: @ticket[:description],
      labels: [@ticket[:team], @ticket[:severity]]
    }

    result = update_ticket params[:id], parameters

    unless result.nil? or result[:errors].nil?
      @errors = result[:errors]
      @ticket = result[:ticket]
      @ticket["id"] = params[:id]
      @ticket["name"] = parameters[:name]
      @ticket["description"] = parameters[:description]
      @ticket["labels"] = []

      parameters[:labels].each do |label|
        @ticket["labels"].push({"name" => label})
      end

      render 'edit'
    else
      redirect_to ticket_path id: params[:id]
    end
  end

  # GET: shows the content of a ticket
  def show
    # add the user ID to know who wrote this message
    @ticket = get_ticket params[:id]
    ticket = Ticket.find_by ticket_id: params[:id]

    @statuses = Ticket.all_statuses
    @alias_statuses = Ticket.alias_all_statuses

    @errors = nil
    @comment = {
      "text" => "",
      "attachments" => ""
    }

    @user = nil
    unless ticket.nil?
      @user = User.find_by id: ticket[:user_id] unless ticket['user_id'].nil?

      if @user.nil? and @ticket["owners"].length > 0
        @user = {
          name: @ticket["owners"][0]["name"],
          email: @ticket["owners"][0]['email']
        }
      end
    end
  end

  # POST: a user sends a comment
  def comment
    data = {
      project_id: params[:ticket][:project_id],
      story_id: params[:ticket][:id],
      text: params[:comment][:text]
    }

    unless params[:comment][:file_attachments].nil? or params[:comment][:file_attachments] == "" or params[:comment][:file_attachments] ==  "[]"
      data[:file_attachments] = ActiveSupport::JSON.decode(params[:comment][:file_attachments])
      data[:text] = "Attached a file" if data[:text].empty?
    end

    result = store_comment(data)

    # if it didn't work for any reason, the page will be rendered instead of
    # redirecting, because PT accepts huge lengths of characters and overflows the cookie size
    unless result[:errors].nil?
      flash = result
      @errors = result[:errors]
      @comment = {
        "text" => params[:comment][:text],
        "attachments" => params[:comment][:file_attachments]
      }

      # add the user ID to know who wrote this message
      @ticket = get_ticket params[:ticket][:id]
      ticket = Ticket.find_by ticket_id: params[:ticket][:id]

      @statuses = Ticket.all_statuses
      @alias_statuses = Ticket.alias_all_statuses

      @user = nil
      unless ticket.nil?
        @user = User.find_by id: ticket[:user_id] unless ticket['user_id'].nil?

        if @user.nil? and @ticket["owners"].length > 0
          @user = {
            name: @ticket["owners"][0]["name"],
            email: @ticket["owners"][0]['email']
          }
        end
      end

      render 'show'
    else
      redirect_to ticket_path id: params[:ticket][:id]
    end

  end

  # Uploads the files to the Pivotal Tracker server and returns the data
  def upload
    attachments = params[:attachments]
    list = []
    result = []
    unless attachments.is_a?(Array)
      list.push attachments
    else
      list = attachments
    end

    list.each do |file|
      path_data = file.tempfile.path.split '/'
      path_data[path_data.length-1] = file.original_filename

      f = File.join("public", file.original_filename)
      FileUtils.cp file.tempfile.path, f
      tfile = File.open(f)

      result.push upload_file tfile, true
    end

    render :json => result.to_json
  end

  # GET: retrieves a file from PT and sends it to the user
  def download
    # if the ID exists, download the file and then will be sent
    file_data = download_file params[:id]
    extension = file_data[:filename].split('.').last
    file = Tempfile.new([file_data[:filename], ".#{extension}"], Rails.root.join('tmp'))
    file.binmode
    file.write(file_data[:content])
    file.close

    send_file file.path, disposition: 'inline'
  end

  private

  # POST: checks if a user is logged in
  def check_login
    if cookies[:auth_token].nil? or session[:user].nil?
      cookies.delete(:auth_token)
      session[:user] = nil
      redirect_to login_path
    end

    if not session[:user].nil?
      access_token = AccessToken.find_by({token: cookies[:auth_token], user_id: session[:user]["id"]})
      if access_token.nil?
        cookies.delete(:auth_token)
        session[:user] = nil
        redirect_to login_path
      end
    end

  end

  def default_header
    Unirest.default_header('X-TrackerToken', ENV['PIVOTAL_TOKEN'])
    Unirest.default_header('Content-Type', 'application/json')
  end
end
