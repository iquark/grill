class Ticket < ActiveRecord::Base
  belongs_to :user

  has_many :user_tickets
  has_many :users, through: :user_tickets

  has_many :transactions

  has_many :tickets, :class_name => "TicketLinked", :foreign_key => :ticket_id
  has_many :followers, :class_name => "TicketLinked", :foreign_key => :follower_id

  @@opened_statuses = ['unscheduled', 'unstarted', 'started', 'finished', 'delivered']
  cattr_reader :opened_statuses

  @@closed_statuses = ['accepted', 'rejected']
  cattr_reader :closed_statuses

  @@all_statuses = Array.new @@opened_statuses
  @@all_statuses.concat @@closed_statuses
  cattr_reader :all_statuses

  # aliases for the statuses in case their names can drive to confussion
  @@alias_opened_statuses = ['unscheduled', 'unstarted', 'started', 'finished', 'delivered']
  cattr_reader :alias_opened_statuses

  @@alias_closed_statuses = ['solved', 'rejected']
  cattr_reader :alias_closed_statuses

  @@alias_all_statuses = Array.new @@alias_opened_statuses
  @@alias_all_statuses.concat @@alias_closed_statuses
  cattr_reader :alias_all_statuses

  @@max_name = 3000
  cattr_reader :max_name

  @@max_description = 15000
  cattr_reader :max_description

  @@max_comment = 15000
  cattr_reader :max_comment

  @@severities = ['low', 'medium', 'high']
  cattr_reader :severities

  @@icon = [":smile:", ":sweat_smile:", ":sweat:"]
  cattr_reader :icon

  def self.get_icon(severity)
    @@icon[@@severities.find_index(severity)]
  end

  def self.validate(name, description)
    errors = nil

    if name.length == 0 or name.length > @@max_name or description.length == 0 or description.length > @@max_description
      errors = []
      if description.length > @@max_description
        errors.push({"field" => "description", "problem" => "Description can not exceed #{@@max_description} characters."})
      elsif description.length == 0
        errors.push({"field" => "description", "problem" => "Description is required."})
      end

      if name.length > @@max_name
        errors.push({"field" => "name", "problem" => "The title can not exceed #{@@max_name} characters."})
      elsif name.length == 0
        errors.push({"field" => "name", "problem" => "The title is required."})
      end
    end

    {errors: errors}
  end
end
