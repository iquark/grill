class User < ActiveRecord::Base

  has_many :user_tickets
  has_many :tickets, through: :ticket_users

  validates :google_id, uniqueness: true
  validates :email, uniqueness: true

  def self.from_omniauth(access_token)
    data = access_token.info
    user = User.find_by(email: data["email"])

    unless user
      members = Unirest.get("#{Team.endpoint}/accounts/#{ENV['PIVOTAL_ACCOUNT']}/memberships").body
      member = members.select {|m| m['person']['email'] == data['email'] }
      member = member.first

      user = User.create(name: data['name'],
                         email: data['email'],
                         google_id: access_token['uid'],
                         pivotal_id: (unless member['id'].nil? then member['id'] else nil end),
                         image: data['image'])
    else
      if user['pivotal_id'].nil?
        # tries to get the PT user's ID if exists
        members = Unirest.get("#{Team.endpoint}/accounts/#{ENV['PIVOTAL_ACCOUNT']}/memberships").body
        unless members.nil?
          member = members.select {|m| m['person']['email'] == data['email'] }
          member = member.first
          user['pivotal_id'] = member['id'] unless member.nil? or member['id'].nil?
          user.save
        end
      end
    end

    user
  end
end
