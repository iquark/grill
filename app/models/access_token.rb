class AccessToken < ActiveRecord::Base
  belongs_to :user

  before_validation :generate_access_token

  validates :user_id, presence: true
  validates :token, presence: true

  private

  def generate_access_token
    self.token = SecureRandom.hex(64)
  end
end
