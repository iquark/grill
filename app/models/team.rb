class Team
  
  @@teams = ['web', 'platform', 'ios', 'android']
  cattr_reader :teams
  @@endpoint = "https://www.pivotaltracker.com/services/v5"
  cattr_reader :endpoint
end