class TicketLinked < ActiveRecord::Base

  belongs_to :ticket, :class_name => "Ticket", :foreign_key => :ticket_id
  belongs_to :follower, :class_name => "Ticket", :foreign_key => :follower_id

end
