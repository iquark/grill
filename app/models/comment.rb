class Comment < ActiveRecord::Base

  belongs_to :user
  belongs_to :ticket

  validates :comment_id, uniqueness: true, null: false
end
