# User follows a ticket
class UserTicket < ActiveRecord::Base
  belongs_to :ticket
  belongs_to :user
end
