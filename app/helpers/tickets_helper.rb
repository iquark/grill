module TicketsHelper
  def time_diff(last, now = nil)
    now = Time.now if now.nil?

    diff = now - last

    if (diff / 1.year).round > 0
      years = (diff / 1.year).round
      "#{years} year#{'s' if years>1}"
    elsif (diff / 1.month).round > 0
      months = (diff / 1.month).round
      "#{months} month#{'s' if months>1}"
    elsif (diff / 1.day).round > 0
      days = (diff / 1.day).round
      "#{days} day#{'s' if days>1}"
    elsif (diff / 1.hour).round > 0
      hours = (diff / 1.hour).round
      "#{hours} hour#{'s' if hours>1}"
    elsif (diff / 1.minute).round > 0
      minutes = (diff / 1.minute).round
      "#{minutes} minute#{'s' if minutes>1}"
    else
      seconds = (diff / 1.second).round
      "#{seconds} second#{'s' if seconds!=1}"
    end

  end
end
