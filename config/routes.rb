Rails.application.routes.draw do

  get  '/auth/:provider/callback', to: 'sessions#create'
  post '/auth/:provider/callback', to: 'sessions#create'
  get  '/auth/failure', to: 'sessions#failure'
  get  '/auth/logout', to: 'sessions#destroy'

  resources :tickets

  get  '/', to: 'tickets#index'
  get  '/css', to: 'css#index'
  post '/tickets/comment', to: 'tickets#comment'
  post '/tickets/upload', to: 'tickets#upload'
  get  '/tickets/download/:id', to: 'tickets#download', as: :download_attachment
  get  'webhook' => 'webhooks#index', as: :webhook
  post 'webhook' => 'webhooks#new', as: :webhook_new
  get  'login' => 'users#index', as: :login
  get  'logout' => 'sessions#destroy', as: :logout
  post 'check-login' => 'users#check_login', as: :check_login

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
