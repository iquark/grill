Rails.application.config.middleware.use OmniAuth::Builder do
  provider :developer unless Rails.env.production?
  provider :google_oauth2, ENV['GOOGLE_OAUTH_KEY'], ENV['GOOGLE_OAUTH_SECRET'], {
    name: 'google_oauth2',
    scope: 'email, profile',
    prompt: 'consent',
    hd: 'unii.com'
  }
end
